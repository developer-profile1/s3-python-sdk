#!/usr/bin/env python
#-*- coding: utf-8 -*-
import boto3
session = boto3.session.Session()
s3 = session.client(
    service_name='s3',
    endpoint_url='https://storage.yandexcloud.net'
)

bucketName = "bucket-python-sdk"

# Создать новый бакет
s3.create_bucket(Bucket=bucketName)

# Загрузить объекты в бакет

## Из строки
s3.put_object(Bucket=bucketName, Key='str_object_name', Body='TEST', StorageClass='STANDARD')

## Из файла
s3.upload_file('/home/user/Documents/s3-python-sdk/py_script.py', bucketName, 'py_script.py')
s3.upload_file('/home/user/Documents/s3-python-sdk/py_script.py', bucketName, 'script/py_script.py')

# Получить список объектов в бакете
for key in s3.list_objects(Bucket=bucketName)['Contents']:
    print(key['Key'])

# Удалить несколько объектов
forDeletion = [{'Key':'object_name'}, {'Key':'script/py_script.py'}]
response = s3.delete_objects(Bucket=bucketName, Delete={'Objects': forDeletion})

# Получить объект
get_object_response = s3.get_object(Bucket=bucketName,Key='py_script.py')
print(get_object_response['Body'].read())
